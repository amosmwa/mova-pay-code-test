'use strict';
var express 				= require('express'),
	app 					= express(),
	bcrypt 					= require('bcrypt-nodejs'),
	models 					= require('../../models'),
	Paginator 				= require('./../paginator'),
	_ 						= require('lodash'),
	topping 				= models.topping,
	toppingType 			= models.toppingType,	
	toppingController		= express.Router();

	toppingController
		.get('/:id?', (req, res)=>{
			if(!_.isEmpty(req.params.id)){
				var id = req.params.id;

				topping
					.findOne({
						where : {
							id : id
						},
						include : [{model : toppingType}]						
					})
					.then((result)=>{
						res.status(200).json(result);
					})
			}else if(!_.isEmpty(req.query.page) && !_.isEmpty(req.query.limit)){
				var query = req.query, page, per_page, skip = null, limit = null, paginator = null;

				page = Number(query.page || 1);
				per_page = Number(query.limit || 10);

				paginator = new Paginator(page, per_page);

				limit = paginator.getLimit();
				skip = paginator.getOffset();

				var where = null
				if(search){
					where = {where : {name :  { $like : "%"+query.text+"%"}}};
				}

				topping
				    .findAndCountAll({offset : skip, limit : limit, include : [{model : toppingType}]})
				    .then((result)=>{
			            var count = result.count;
			            var rows = result.rows;

			            paginator.setCount(count);
			            paginator.setData(rows);

			            res.status(200).json(paginator.getPaginator());
				    })
				    .catch(err=>{ res.status(500).send(err); });	
			}else{
				topping.findAll()
				.then(result=>{
					res.status(200).json(result);
				})
			}
		})
		.post('/', (req, res)=>{
			if(!_.isEmpty(req.body)){

				var data = req.body;

				topping
					.create({
						name 			: data.name,						
						small 			: data.small,
						medium 			: data.medium,
						large 			: data.large,
						toppingTypeId 	:  data.toppingType.id
					})			
					.then(function(result){
						res.status(200).send();
					})
			}else{
				res.status(401).json({message : 'request has no body'});
			}
		})
		.put('/', (req, res)=>{
			if(!_.isEmpty(req.body)){

				var data = req.body;

				topping
					.update({
						name 			: data.name,						
						small 			: data.small,
						medium 			: data.medium,
						large 			: data.large,
						toppingTypeId 	:  data.toppingType.id
					}, {
						where : {
							id : data.id
						}
					})			
					.then(function(result){
						res.status(200).send();
					})
			}else{
				res.status(401).json({message : 'request has no body'});
			}
		})
		.delete('/', (req, res)=>{

		})

module.exports = toppingController;