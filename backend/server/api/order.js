'use strict';
var express 				= require('express'),
	app 					= express(),
	bcrypt 					= require('bcrypt-nodejs'),
	models 					= require('../../models'),
	Paginator 				= require('./../paginator'),
	_ 						= require('lodash'),
	order 					= models.order,	
	orderController			= express.Router();

	orderController
		.get('/:id?/:orderLineItem?', (req, res)=>{
			if(!_.isEmpty(req.params.id)){
				order
					.findOne({
						where : {id : req.params.id},
						include : [{model : models.orderLineItem, include : [{model : models.pizza},{model : models.toppingOrderLineItem, include : [{model : models.topping, include : [models.toppingType]}]}]},]
					})
					.then(result=>{
						res.status(200).json(result);
					})
			}else if(!_.isEmpty(req.query.page) && !_.isEmpty(req.query.limit)){

				var query = req.query;

				var page, per_page, skip = null, limit = null, paginator = null;

				page = Number(query.page || 1);
				per_page = Number(query.limit || 10);

				paginator = new Paginator(page, per_page);

				limit = paginator.getLimit();
				skip = paginator.getOffset();

				order
					.findAndCountAll({offset : skip, limit : limit, 
						include : [{model : models.user},{model : models.orderLineItem, include : [{model : models.pizza},{model : models.toppingOrderLineItem, include : [{model : models.topping, include : [models.toppingType]}]}]}],
						order 	: [['createdAt', 'DESC']],
					})
					.then(results=>{
						var count = results.count;
						var rows = results.rows;

						paginator.setCount(count);
						paginator.setData(rows);

						res.status(200).json(paginator.getPaginator());
					})
			}
		})
		.post('/', (req, res)=>{
			if(!_.isEmpty(req.body)){
				var data = req.body;
				models.user.findAll()
					.then(result=>{
						var user = result[Math.floor(Math.random() * result.length)]

						return order.create({
								userId : user.id,
								gst : data.gst,
								subtotal : data.subtotal
							})	
					})
					.then(result=>{
						var orderId = result.get('id');

						return models.sequelize.Promise.each(data.orderLineItems, entry=>{
							return models.orderLineItem
							.create({
								orderId : orderId,
								pizzaId : entry.pizza.id,
								pizzaPrice : entry.pizza.price,
								quantity : entry.quantity,
								subtotal : entry.subtotal
							})
							.then(result=>{
								var orderLineItemId = result.get('id');
								return models.sequelize.Promise.each(entry.toppingOrderLineItems, _entry=>{
									return models.toppingOrderLineItem
									.create({
										orderLineItemId : orderLineItemId,
										toppingId : _entry.id,
										toppingPrice : _entry[''+entry.pizza.name]
									})
								})
							})
						})
					})
					.then(result=>{	
						res.status(200).send();
					})
					.catch(err=>{
						res.status(500).send(err);
					})
			}
		});

module.exports = orderController;		