'use strict';
var express 				= require('express'),
	app 					= express(),
	bcrypt 					= require('bcrypt-nodejs'),
	models 					= require('../../models'),
	Paginator 				= require('./../paginator'),
	_ 						= require('lodash'),
	pizza 					= models.pizza,	
	pizzaController			= express.Router();


pizzaController
	.get('/:id?', (req, res)=>{
		if(!_.isEmpty(req.params.id)){
			var id = req.params.id;

			pizza
				.findOne({
					where : {
						id : id
					}					
				})
				.then((result)=>{
					res.status(200).json(result);
				})
		}else if(!_.isEmpty(req.query.page) && !_.isEmpty(req.query.limit)){
			var query = req.query, page, per_page, skip = null, limit = null, paginator = null;

			page = Number(query.page || 1);
			per_page = Number(query.limit || 10);

			paginator = new Paginator(page, per_page);

			limit = paginator.getLimit();
			skip = paginator.getOffset();

			var where = null
			if(search){
				where = {where : {name :  { $like : "%"+query.text+"%"}}};
			}

			pizza
			    .findAndCountAll({offset : skip, limit : limit, where : where})
			    .then((result)=>{
		            var count = result.count;
		            var rows = result.rows;

		            paginator.setCount(count);
		            paginator.setData(rows);

		            res.status(200).json(paginator.getPaginator());
			    })
			    .catch(err=>{ res.status(500).send(err); });	
		}else{
			pizza.findAll()
			.then(result=>{
				res.status(200).json(result);
			})
		}
	})
	.post('/', (req, res)=>{
		if(!_.isEmpty(req.body)){

			var data = req.body;

			pizza
				.create({
					name : data.name,
					price : data.price
				})			
				.then(function(result){
					res.status(200).send();
				})
		}else{
			res.status(401).json({message : 'request has no body'});
		}
	})
	.put('/', (req, res)=>{
		if(!_.isEmpty(req.body)){

			var data = req.body;

			pizza
				.update({
					name : data.name,
					price : data.price
				}, {
					where : {
						id : data.id
					}
				})			
				.then(function(result){
					res.status(200).send();
				})
		}else{
			res.status(401).json({message : 'request has no body'});
		}
	})
	.delete('/', (req, res)=>{

	})

module.exports = pizzaController;