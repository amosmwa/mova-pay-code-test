'use strict';
var express 				= require('express'),
	app 					= express(),
	bcrypt 					= require('bcrypt-nodejs'),
	models 					= require('../../models'),
	Paginator 				= require('./../paginator'),
	_ 						= require('lodash'),
	user 					= models.user,
	userRole 				= models.userRole,
	role 					= models.role,	
	userController			= express.Router();

userController
	.get('/:id?', (req, res)=>{
		if(!_.isEmpty(req.params.id)){
			var id = req.params.id;

			user
				.findOne({
					where : {
						id : id
					},
					include : [{model : userRole, include : [role]}]
				})
				.then((result)=>{
					res.status(200).json(result);
				})
		}else if(!_.isEmpty(req.query.page) && !_.isEmpty(req.query.limit)){
			findAndCountAll(res, req.query, false)
		}else if(!_.isEmpty(req.query.search)){
			findAndCountAll(res, req.query, true)
		}else{
			user.findAll()
			.then(result=>{
				res.status(200).json(result);
			})
		}
	});

	userController
		.post('/changePassword', function(req, res){
			if(req.body){
				var data = req.body,
				salt  = bcrypt.genSaltSync(10);
				user
					.update({
						password : bcrypt.hashSync(data.confirm, salt)
					},{
						where  : {
							id : data.userId
						}
					})			
					.then(function(result){
						res.status(200).send();
					})
			}else{
				res.status(401).json({message : 'request has no body'});
			}
		});

	userController
		.post('/',function(req, res){		
			if(req.body){
				var data = req.body,
				salt  = bcrypt.genSaltSync(10);

				
			}else{
				res.status(401).json({message : 'request has no body'});
			}		
		})

	userController
		.put('/', (req, res)=>{
			if(req.body){
				var data = req.body

				
			}else{
				res.status(401).json({message : 'request has no body'});
			}
		});




	userController
		.delete('/:id', function(req, res){
			
		});

	function findAndCountAll(res, query, search){
		var page, per_page, skip = null, limit = null, paginator = null;

		page = Number(query.page || 1);
		per_page = Number(query.limit || 10);

		paginator = new Paginator(page, per_page);

		limit = paginator.getLimit();
		skip = paginator.getOffset();

		var where = null
		if(search){
			where = {where : {name :  { $like : "%"+query.text+"%"}}};
		}

		user
		    .findAndCountAll({offset : skip, limit : limit, where : where})
		    .then((result)=>{
	            var count = result.count;
	            var rows = result.rows;

	            paginator.setCount(count);
	            paginator.setData(rows);

	            res.status(200).json(paginator.getPaginator());
		    })
		    .catch(err=>{ res.status(500).send(err); });
	}


module.exports = userController;
