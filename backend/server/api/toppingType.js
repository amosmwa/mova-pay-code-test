'use strict';
var express 				= require('express'),
	app 					= express(),
	models 					= require('../../models'),
	Paginator 				= require('./../paginator'),
	_ 						= require('lodash'),
	toppingType 			= models.toppingType,
	toppingTypeController	= express.Router();

toppingTypeController.route('/:id?')
	.get(function(req, res){
		if(req.params.id){
			var id = req.params.id;

			toppingType
				.findOne({
					where : {
						id : id
					}
				})
				.then(function(result){
					res.status(200).json(result);
				})
		}else if(req.query.toppings){
			var query = req.query;

			if(!_.isEmpty(query.toppings) && query.toppings){
				toppingType
					.findAll({include : [{model : models.topping}]})
					.then(result=>{
						res.status(200).json(result);
					})
					.catch(err=>{
						console.log(err);
					})
			}
		}else if(req.query.limit && req.query.page){
				var query = req.query;

				var page, per_page, skip = null, limit = null, paginator = null;

				page = Number(query.page || 1);
				per_page = Number(query.limit || 10);

				paginator = new Paginator(page, per_page);

				limit = paginator.getLimit();
				skip = paginator.getOffset();


				toppingType
				    .findAndCountAll({offset : skip, limit : limit})
				    .then(function(result){
				            var count = result.count;
				            var rows = result.rows;

				            paginator.setCount(count);
				            paginator.setData(rows);

				            res.status(200).json(paginator.getPaginator());

				            //log.read(app.get('userId'), 'get paginated case adjournment reason');

				    });
			}else{
				toppingType
					.findAll()
					.then(function(result){
						res.status(200).json(result);
					})
					.catch(function(error){
						res.status(500).send(error);
					});
			}
	})
	.post(function(req, res){
		if(req.body){
			var data = req.body

			toppingType
				.create({
					name : data.name
				})
				.then(function(result){
					res.status(201).send();
				})
		}else{
			res.status(401).json({message : 'request has no body'});
		}
	})
	.put(function(req, res){
		if(req.body){
			var data = req.body

			toppingType
				.update({
					name : data.name
				}, {
					where : {
						id : data.id
					}
				})
				.then(function(result){
					res.status(201).send();
				})
		}else{
			res.status(401).json({message : 'request has no body'});
		}
	})
	.delete(function(req, res){

	});

module.exports = toppingTypeController;