'use strict';
var express = require('express'),
 app		= require('express')(),
 path		= require('path'),
 bodyParser = require('body-parser'),
 server		= '',
 bcrypt = require('bcrypt-nodejs'),
 dotenv = require('dotenv').config(), 
 jwt    = require('jwt-simple'),
 moment = require('moment'),
 secret = process.env.SECRET, 
 models = require(path.resolve(__dirname + '\\..\\models'));

 models.order.hasMany(models.orderLineItem);
 models.order.belongsTo(models.user);
 models.orderLineItem.belongsTo(models.order);
 models.orderLineItem.belongsTo(models.pizza);
 models.orderLineItem.hasMany(models.toppingOrderLineItem);
 models.pizza.hasMany(models.orderLineItem);
 models.topping.belongsTo(models.toppingType);
 models.toppingOrderLineItem.belongsTo(models.orderLineItem);
 models.toppingOrderLineItem.belongsTo(models.topping);
 models.toppingType.hasMany(models.topping); 
 models.user.hasMany(models.order);
 models.userRole.belongsTo(models.user); 
 models.userRole.belongsTo(models.role); 

models.sequelize.sync().then(()=>{ server = app.listen(8081); console.log("Biling System Started");});

app.use(express.static(path.resolve(__dirname + './../public')));

app.use(function (req, res, next) {    

    // Website you wish to allow to connect    
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Headers,  X-Access-Token');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.json());  

function authError(res){
    res.status(401).json({message : "email or password invalid"});
}

app.post('/login', function(req, res){
    if (_.isEmpty(req.body)) {
        authError(res);
    }
    var data = req.body,
    authed = {};

    user
        .findOne({               
            include : [{ model : models.userRole, attributes : ['id', 'roleId'], include : [{model : models.role, attributes : ['name']}]}],
            where : {'email' : data.email}
        })
        .then(function(result){
            if(result && bcrypt.compareSync(data.password,result.password)){                                
                var ret = {_id : result.id, id : result.id, displayName : result.name, username : result.email, roleId : result.userRole.id, role : result.userRole.role.name};
                return ret;                    
            }
            authError(res);
        })
        .then(function(result){
            result.issuedAt = new Date();
            var token = jwt.encode(result, secret);
            result.token = token;
            authed = result;

            return result;
        })
        .then(function(result){
            return user.update({
                    lastAccessTime : new Date()
                },{
                    where  :{
                        id : result.id
                    }
                }); 
        })
        .then(function(result){
            res.status(200).json(authed);
        })
        .catch(function(err){            
            res.status(500).send(err);
        });
});

app.post('/newPass', function(req, res){
    if(_.isEmpty(req.body)){
        res.status(403).send('error occured');
    }
    var data = req.body;

    models.user
        .update({
            password    : bcrypt.hashSync(data.confirm),
            resetCode   : ''
        },{
            where : {
                resetCode : data.uuid
            }
        })
        .then(function(result){
            res.status(200).send();
        })
        .catch(function(error){
            res.status(500).send('error occured');
        });                
});

/*app.post('/forgotPass', function(req, res){
    if(_.isEmpty(req.body)){
        res.status(403).send('error occured');
    }

    models.user
        .findOne({
            where : {
                email : req.body.email
            }
        })
        .then(function(result){           
            var secret = uuid.v4(),
            userId = result.id;

            
        });    
});*/


require('./api')(app);
