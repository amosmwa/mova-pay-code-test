'use strict';
module.exports = function(sequelize, DataTypes) {
  var orderLineItem = sequelize.define('orderLineItem', { 
  		 id      : {
          type      : DataTypes.UUID,
          defaultValue  : DataTypes.UUIDV4,
          primaryKey    : true
        },
        orderId : DataTypes.UUID,
        pizzaId : DataTypes.UUID,
        quantity : DataTypes.INTEGER,
        pizzaPrice : DataTypes.DOUBLE,
        subtotal : DataTypes.DOUBLE
   }, {
    classMethods: {
      associate: function(models) {
        orderLineItem.belongsTo(models.order);
        orderLineItem.belongsTo(models.pizza);
      }
    }
  });
  return orderLineItem;
};