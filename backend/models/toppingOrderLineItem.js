'use strict';
module.exports = function(sequelize, DataTypes) {
  var toppingOrderLineItem = sequelize.define('toppingOrderLineItem', { 
  		 id      : {
          type      : DataTypes.UUID,
          defaultValue  : DataTypes.UUIDV4,
          primaryKey    : true
        },
        orderLineItemId : DataTypes.UUID,
        toppingId : DataTypes.UUID,
        toppingPrice :  DataTypes.DOUBLE
   }, {
    classMethods: {
      associate: function(models) {
        toppingOrderLineItem.belongsTo(models.orderLineItem);
        toppingOrderLineItem.belongsTo(models.topping);
      }
    }
  });
  return toppingOrderLineItem;
};