'use strict';
module.exports = function(sequelize, DataTypes) {
  var pizza = sequelize.define('pizza', { 
  		 id      : {
          type      : DataTypes.UUID,
          defaultValue  : DataTypes.UUIDV4,
          primaryKey    : true
        },
        name : DataTypes.STRING,
        price : DataTypes.DOUBLE
   }, {
    classMethods: {
      associate: function(models) {
        pizza.hasMany(models.orderLineItem);
      }
    }
  });
  return pizza;
};