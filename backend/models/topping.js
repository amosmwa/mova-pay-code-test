'use strict';
module.exports = function(sequelize, DataTypes) {
  var topping = sequelize.define('topping', { 
  		 id      : {
          type      : DataTypes.UUID,
          defaultValue  : DataTypes.UUIDV4,
          primaryKey    : true
        },
        name : DataTypes.STRING,
        small : DataTypes.DOUBLE,
        medium : DataTypes.DOUBLE,
        large : DataTypes.DOUBLE,
        toppingTypeId : DataTypes.UUID
   }, {
    classMethods: {
      associate: function(models) {
        topping.belongsTo(models.toppingType);
      }
    }
  });
  return topping;
};