'use strict';
module.exports = function(sequelize, DataTypes) {
  var order = sequelize.define('order', { 
  		 id      : {
          type      : DataTypes.UUID,
          defaultValue  : DataTypes.UUIDV4,
          primaryKey    : true
        },
        userId : DataTypes.UUID,        
        subtotal : DataTypes.DOUBLE,
        gst : DataTypes.DOUBLE
   }, {
    classMethods: {
      associate: function(models) {
        order.hasMany(models.orderLineItem);
        order.belongsTo(models.user);
      }
    }
  });
  return order;
};