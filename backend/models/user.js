'use strict';
module.exports = function(sequelize, DataTypes) {
  var user = sequelize.define('user', {
      id      : {
        type      : DataTypes.UUID,
        defaultValue  : DataTypes.UUIDV4,
        primaryKey    : true
      },
      name  : DataTypes.STRING,
      email   : {
        type : DataTypes.STRING,
        allowNull : true
      },    
      gender  : DataTypes.BOOLEAN,
      password      : DataTypes.STRING,
      lastLoginDate : DataTypes.DATE,
      active : {
        type : DataTypes.BOOLEAN,
        defaultValue : false
      },
      resetCode : {
        type : DataTypes.STRING,
        allowNull : true
      }          
   }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        user.hasMany(models.order);
      }
    }
  });
  return user;
};