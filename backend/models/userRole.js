'use strict';
module.exports = function(sequelize, DataTypes) {
  var userRole = sequelize.define('userRole', { 
  		  roleId  : DataTypes.UUID,
      	userId  : DataTypes.UUID
   }, {
    classMethods: {
      associate: function(models) {
         userRole.belongsTo(models.user); 
			   userRole.belongsTo(models.role); 
      }
    }
  });
  return userRole;
};