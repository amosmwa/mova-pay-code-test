'use strict';
module.exports = function(sequelize, DataTypes) {
  var role = sequelize.define('role', { 
  		id      : {
  		  type      : DataTypes.UUID,
  		  defaultValue  : DataTypes.UUIDV4,
  		  primaryKey    : true
  		},     
      	name : DataTypes.STRING
   }, {
    classMethods: {
      associate: function(models) {
			         
      }
    }
  });
  return role;
};