'use strict';
module.exports = function(sequelize, DataTypes) {
  var toppingType = sequelize.define('toppingType', { 
  		 id      : {
          type      : DataTypes.UUID,
          defaultValue  : DataTypes.UUIDV4,
          primaryKey    : true
        },
        name : DataTypes.STRING
   }, {
    classMethods: {
      associate: function(models) {
        toppingType.hasMany(models.topping);
        toppingType.hasMany(models.toppingOrderLineItem);        
      }
    }
  });
  return toppingType;
};