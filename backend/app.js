'use strict';
var Sequelize = require('sequelize'),
	bcrypt = require('bcrypt-nodejs');

var connection = new Sequelize('mova', 'root', '',{timezone : 'Africa/Nairobi', port : "3306", host: "127.0.0.1", dialect : "mysql"});

var user = connection.define('user', {
	id      : {
		type 			: Sequelize.UUID,
		defaultValue 	: Sequelize.UUIDV4,
		primaryKey 		: true
	},
	name 	: Sequelize.STRING,
	email 	: {
		type : Sequelize.STRING,
		allowNull : true
	},		
	gender 	: Sequelize.BOOLEAN,
	password 		:  Sequelize.STRING,
	lastLoginDate 	: Sequelize.DATE,
	active : {
		type : Sequelize.BOOLEAN,
		defaultValue : false
	},
	resetCode : {
		type : Sequelize.STRING,
		allowNull : true
	},
});

var role = connection.define('role', {
	id      : {
		type 			: Sequelize.UUID,
		defaultValue 	: Sequelize.UUIDV4,
		primaryKey 		: true
	},
	name 	: Sequelize.STRING
});

var userRole = connection.define('userRole', {
	userId 	: Sequelize.UUID,
	roleId	: Sequelize.UUID 
});

var pizza = connection.define('pizza', {
	id      : {
		type 			: Sequelize.UUID,
		defaultValue 	: Sequelize.UUIDV4,
		primaryKey 		: true
	},
	name : Sequelize.STRING,
	price : Sequelize.DOUBLE
});

var toppingType = connection.define('toppingType', {
	id      : {
		type 			: Sequelize.UUID,
		defaultValue 	: Sequelize.UUIDV4,
		primaryKey 		: true
	},
	name : Sequelize.STRING
});

var topping = connection.define('topping',{
	id      : {
		type 			: Sequelize.UUID,
		defaultValue 	: Sequelize.UUIDV4,
		primaryKey 		: true
	},
	name : Sequelize.STRING,
	small : Sequelize.DOUBLE,
	medium : Sequelize.DOUBLE,
	large : Sequelize.DOUBLE,
	toppingTypeId : Sequelize.UUID,	
});

var order = connection.define('order', {
	id      : {
		type 			: Sequelize.UUID,
		defaultValue 	: Sequelize.UUIDV4,
		primaryKey 		: true
	},
	userId : Sequelize.UUID,
	gst : Sequelize.DOUBLE,	
	subtotal : Sequelize.DOUBLE 
});

var orderLineItem = connection.define('orderLineItem', {
	id      : {
		type 			: Sequelize.UUID,
		defaultValue 	: Sequelize.UUIDV4,
		primaryKey 		: true
	},
	orderId : Sequelize.UUID,
	pizzaId : Sequelize.UUID,
	quantity : Sequelize.INTEGER,
	pizzaPrice : Sequelize.DOUBLE,
	subtotal : Sequelize.DOUBLE,	
});

var toppingOrderLineItem = connection.define('toppingOrderLineItem', {
	id      : {
		type 			: Sequelize.UUID,
		defaultValue 	: Sequelize.UUIDV4,
		primaryKey 		: true
	},
	orderLineItemId : Sequelize.UUID,
	toppingId : Sequelize.UUID,
	toppingPrice :  Sequelize.DOUBLE	 
});


connection.sync({
	logging : true,
	force 	: true
}).then(function() {
	//createUser();	
	createRole();
	createPizza();
	createToppingType();
});


function createRole(){
	var users = [], roles = [];
	role.bulkCreate([
		{name : "USER"},
		{name : "ADMIN"}
	])
	.then(re=>{
		return role.findAll();
	})
	.then(re=>{
		roles = re;
		users  = [
			{name : "Amos Mwangi", email : "amosmwa@gmail.com", gender : true, active: true, password : bcrypt.hashSync("12345")},
			{name : "Billy Ochingwa", email : "billy.ochingwa@gmail.com", gender : true, active: true, password : bcrypt.hashSync("12345")},
			{name : "Edith Kinya", email : "edith.kinya@gmail.com", gender : true, active: true, password : bcrypt.hashSync("12345")}
		]

		return Sequelize.Promise.each(users, (entry, index)=>{
			return user.create(entry)
			.then(re=>{
				var i = index == true ? 1 : 0;
				return userRole.create({userId : re.get('id'), roleId : roles[i].id});
			})
		})
	})
}

function createPizza(){
	pizza.bulkCreate([
		{name : "small", price : 12},
		{name : "medium", price : 14},
		{name : "large", price : 16}
	]);
}

function createToppingType(){
	toppingType.bulkCreate([
		{name : "Basic Toppings"},
		{name : "Deluxe Toppings"},
	])
	.then(resultt=>{
		return toppingType.findAll()
	})
	.then(results=>{
		var tt = results;

		topping.bulkCreate([
			{name : "Cheese", small : 0.50, medium : 0.75, large : 1.00, toppingTypeId : tt[0].id},
			{name : "Pepperoni", small : 0.50, medium : 0.75, large : 1.00, toppingTypeId : tt[0].id},
			{name : "Ham", small : 0.50, medium : 0.75, large : 1.00, toppingTypeId : tt[0].id},
			{name : "Pineapple", small : 0.50, medium : 0.75, large : 1.00, toppingTypeId : tt[0].id},

			{name : "Sausage", small : 2.00, medium : 3.00, large : 4.00, toppingTypeId : tt[1].id},
			{name : "Feta Cheese", small : 2.00, medium : 3.00, large : 4.00, toppingTypeId : tt[1].id},
			{name : "Tomatoes", small : 2.00, medium : 3.00, large : 4.00, toppingTypeId : tt[1].id},
			{name : "Olives", small : 2.00, medium : 3.00, large : 4.00, toppingTypeId : tt[1].id}
		])
	});
}

