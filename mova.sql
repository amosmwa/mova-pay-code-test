-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2018 at 12:27 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mova`
--

-- --------------------------------------------------------

--
-- Table structure for table `orderLineItems`
--

CREATE TABLE `orderLineItems` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `orderId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `pizzaId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `pizzaPrice` double DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderLineItems`
--

INSERT INTO `orderLineItems` (`id`, `orderId`, `pizzaId`, `quantity`, `pizzaPrice`, `subtotal`, `createdAt`, `updatedAt`) VALUES
('7904df9f-c476-4213-8536-fb223c3118a2', '370db9bf-3137-4f31-843b-08d14c1588eb', '6c3a9344-d859-43de-bfa3-bacfa8ffbb17', 1, 14, 14, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('af6ee204-b299-4ee4-b1ba-9c77f49b4838', '370db9bf-3137-4f31-843b-08d14c1588eb', '7eb80324-33f8-4be6-a63d-1bfcd092fd70', 1, 16, 22, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('b7874ed4-c1ed-4bbd-ad93-03553e938770', 'e3c11b8f-6434-429f-819d-fd998e5611f6', '7eb80324-33f8-4be6-a63d-1bfcd092fd70', 1, 16, 38, '2018-02-05 10:22:11', '2018-02-05 10:22:11'),
('cdec38a3-e3c9-4c02-a642-71c85f3520f2', '31ff8f9f-d8b8-4561-bdf6-db277fc73889', '6c3a9344-d859-43de-bfa3-bacfa8ffbb17', 1, 14, 23, '2018-02-05 10:13:18', '2018-02-05 10:13:18'),
('e38259f7-c034-4f45-8323-7985e7ccaa16', '31ff8f9f-d8b8-4561-bdf6-db277fc73889', 'f5683cf6-6a0f-48fa-a401-4d666ccaecd0', 1, 12, 18, '2018-02-05 10:13:19', '2018-02-05 10:13:19'),
('ee5711f8-6a81-4110-bb89-8be9bcb619d0', '829ee182-cfce-4108-b48c-2eefd8f14df4', '7eb80324-33f8-4be6-a63d-1bfcd092fd70', 1, 16, 29, '2018-02-05 10:31:04', '2018-02-05 10:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `userId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `gst` double DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `userId`, `gst`, `subtotal`, `createdAt`, `updatedAt`) VALUES
('31ff8f9f-d8b8-4561-bdf6-db277fc73889', 'bdd41557-23b2-48c7-90bd-f5464f29768b', 2.0500000000000003, 41, '2018-02-05 10:13:18', '2018-02-05 10:13:18'),
('370db9bf-3137-4f31-843b-08d14c1588eb', 'd44c1b6a-a804-4dd8-8450-aa6a4093c8a9', 1.8, 36, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('829ee182-cfce-4108-b48c-2eefd8f14df4', 'd44c1b6a-a804-4dd8-8450-aa6a4093c8a9', 1.4500000000000002, 29, '2018-02-05 10:31:04', '2018-02-05 10:31:04'),
('e3c11b8f-6434-429f-819d-fd998e5611f6', 'd44c1b6a-a804-4dd8-8450-aa6a4093c8a9', 1.9000000000000001, 38, '2018-02-05 10:22:11', '2018-02-05 10:22:11');

-- --------------------------------------------------------

--
-- Table structure for table `pizzas`
--

CREATE TABLE `pizzas` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pizzas`
--

INSERT INTO `pizzas` (`id`, `name`, `price`, `createdAt`, `updatedAt`) VALUES
('6c3a9344-d859-43de-bfa3-bacfa8ffbb17', 'medium', 14, '2018-02-05 12:28:35', '2018-02-05 12:28:35'),
('7eb80324-33f8-4be6-a63d-1bfcd092fd70', 'large', 16, '2018-02-05 12:28:35', '2018-02-05 12:28:35'),
('f5683cf6-6a0f-48fa-a401-4d666ccaecd0', 'small', 12, '2018-02-05 12:28:35', '2018-02-05 12:28:35');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `createdAt`, `updatedAt`) VALUES
('368ec8c9-c110-426f-bcc6-9174ac39962e', 'USER', '2018-02-05 12:28:35', '2018-02-05 12:28:35'),
('f6c92029-c513-47e9-ba53-17ea867cd22b', 'ADMIN', '2018-02-05 12:28:35', '2018-02-05 12:28:35');

-- --------------------------------------------------------

--
-- Table structure for table `toppingOrderLineItems`
--

CREATE TABLE `toppingOrderLineItems` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `orderLineItemId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `toppingId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `toppingPrice` double DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toppingOrderLineItems`
--

INSERT INTO `toppingOrderLineItems` (`id`, `orderLineItemId`, `toppingId`, `toppingPrice`, `createdAt`, `updatedAt`) VALUES
('052cf516-d23a-4377-8569-a2638fcb7186', 'cdec38a3-e3c9-4c02-a642-71c85f3520f2', '180fe498-3799-4819-be81-82b8c48d86bb', 0.75, '2018-02-05 10:13:18', '2018-02-05 10:13:18'),
('0f9ae11e-97ee-47ee-ac97-b970a6300acc', 'cdec38a3-e3c9-4c02-a642-71c85f3520f2', '93921403-65ed-4634-8411-3b068460e38b', 3, '2018-02-05 10:13:19', '2018-02-05 10:13:19'),
('1df55c36-e660-4a30-9f5e-352e9f4ccd3c', 'e38259f7-c034-4f45-8323-7985e7ccaa16', '5f796b10-120e-4524-a095-6f66b2580411', 0.5, '2018-02-05 10:13:19', '2018-02-05 10:13:19'),
('2cf64263-eaaa-4228-921b-a5dfb2633fb9', 'e38259f7-c034-4f45-8323-7985e7ccaa16', '55fc59dd-cf38-41f2-ac3b-f799fc55f133', 0.5, '2018-02-05 10:13:19', '2018-02-05 10:13:19'),
('37aad4a3-8e32-44b8-9005-4734fafaa675', 'af6ee204-b299-4ee4-b1ba-9c77f49b4838', '5f796b10-120e-4524-a095-6f66b2580411', 1, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('38a4f08b-d83a-4b75-a076-1c536042ceeb', 'e38259f7-c034-4f45-8323-7985e7ccaa16', '93921403-65ed-4634-8411-3b068460e38b', 2, '2018-02-05 10:13:19', '2018-02-05 10:13:19'),
('460a1c14-ea42-4d4b-aa13-252ecf4dfc39', 'ee5711f8-6a81-4110-bb89-8be9bcb619d0', '50dba3bf-e47b-4272-910d-2647fbfc34f0', 1, '2018-02-05 10:31:04', '2018-02-05 10:31:04'),
('4dcde229-a258-4218-9e62-5f69773e35cd', 'b7874ed4-c1ed-4bbd-ad93-03553e938770', '50dba3bf-e47b-4272-910d-2647fbfc34f0', 1, '2018-02-05 10:22:12', '2018-02-05 10:22:12'),
('51fd03f5-af12-4f99-8a1e-e2e6a9ebf99f', 'ee5711f8-6a81-4110-bb89-8be9bcb619d0', '93921403-65ed-4634-8411-3b068460e38b', 4, '2018-02-05 10:31:04', '2018-02-05 10:31:04'),
('576a3a71-0f96-4371-9dcd-73074243b0f5', 'b7874ed4-c1ed-4bbd-ad93-03553e938770', '55fc59dd-cf38-41f2-ac3b-f799fc55f133', 1, '2018-02-05 10:22:12', '2018-02-05 10:22:12'),
('6fff8bcc-b7c5-44d5-b5b8-1a32e0dc9d73', 'e38259f7-c034-4f45-8323-7985e7ccaa16', 'a701973d-7c55-4050-901b-f762066e9024', 2, '2018-02-05 10:13:19', '2018-02-05 10:13:19'),
('7bb5f8b7-8ed9-4568-bcde-e1762e9bb19b', 'cdec38a3-e3c9-4c02-a642-71c85f3520f2', '50dba3bf-e47b-4272-910d-2647fbfc34f0', 0.75, '2018-02-05 10:13:18', '2018-02-05 10:13:18'),
('816c355b-40d2-4d70-855a-e879c9bbae26', 'ee5711f8-6a81-4110-bb89-8be9bcb619d0', '55fc59dd-cf38-41f2-ac3b-f799fc55f133', 1, '2018-02-05 10:31:04', '2018-02-05 10:31:04'),
('8c05575e-ea6a-435a-83f8-07546d55ade2', '7904df9f-c476-4213-8536-fb223c3118a2', '55fc59dd-cf38-41f2-ac3b-f799fc55f133', 0.75, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('96b60b55-e22c-4808-a510-45bc1a82198e', '7904df9f-c476-4213-8536-fb223c3118a2', '50dba3bf-e47b-4272-910d-2647fbfc34f0', 0.75, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('9fffe247-adc2-40aa-b63e-60118918575c', 'e38259f7-c034-4f45-8323-7985e7ccaa16', '50dba3bf-e47b-4272-910d-2647fbfc34f0', 0.5, '2018-02-05 10:13:19', '2018-02-05 10:13:19'),
('af34af15-252a-4eeb-98aa-8261764968cd', '7904df9f-c476-4213-8536-fb223c3118a2', '180fe498-3799-4819-be81-82b8c48d86bb', 0.75, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('bc12960b-48f5-40f9-b86b-62bf58ffd2b1', '7904df9f-c476-4213-8536-fb223c3118a2', '5f796b10-120e-4524-a095-6f66b2580411', 0.75, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('bee79767-eea4-4c10-a0ac-a00343d833f7', 'b7874ed4-c1ed-4bbd-ad93-03553e938770', '5f796b10-120e-4524-a095-6f66b2580411', 1, '2018-02-05 10:22:12', '2018-02-05 10:22:12'),
('cf4a525a-5432-438b-b1f8-f508c9653448', 'b7874ed4-c1ed-4bbd-ad93-03553e938770', '180fe498-3799-4819-be81-82b8c48d86bb', 1, '2018-02-05 10:22:12', '2018-02-05 10:22:12'),
('d8e735ea-af0a-4801-a089-5becbc7096dc', 'cdec38a3-e3c9-4c02-a642-71c85f3520f2', '8517e0e9-efe8-49cc-9259-3f61af124ebe', 3, '2018-02-05 10:13:18', '2018-02-05 10:13:18'),
('e02c3b25-5179-4d2c-8e63-3d3b10f24890', 'af6ee204-b299-4ee4-b1ba-9c77f49b4838', '55fc59dd-cf38-41f2-ac3b-f799fc55f133', 1, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('e1cdf94d-0ed5-4411-bee8-2a4f58c74d60', 'af6ee204-b299-4ee4-b1ba-9c77f49b4838', '50dba3bf-e47b-4272-910d-2647fbfc34f0', 1, '2018-02-05 10:24:28', '2018-02-05 10:24:28'),
('fb58be88-3049-4800-a140-a1ecddd499da', 'ee5711f8-6a81-4110-bb89-8be9bcb619d0', '180fe498-3799-4819-be81-82b8c48d86bb', 1, '2018-02-05 10:31:04', '2018-02-05 10:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `toppings`
--

CREATE TABLE `toppings` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `small` double DEFAULT NULL,
  `medium` double DEFAULT NULL,
  `large` double DEFAULT NULL,
  `toppingTypeId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toppings`
--

INSERT INTO `toppings` (`id`, `name`, `small`, `medium`, `large`, `toppingTypeId`, `createdAt`, `updatedAt`) VALUES
('180fe498-3799-4819-be81-82b8c48d86bb', 'Cheese', 0.5, 0.75, 1, '31f1fc90-b7c3-46c9-8d01-cccde2c89685', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('50dba3bf-e47b-4272-910d-2647fbfc34f0', 'Pepperoni', 0.5, 0.75, 1, '31f1fc90-b7c3-46c9-8d01-cccde2c89685', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('55fc59dd-cf38-41f2-ac3b-f799fc55f133', 'Pineapple', 0.5, 0.75, 1, '31f1fc90-b7c3-46c9-8d01-cccde2c89685', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('5f796b10-120e-4524-a095-6f66b2580411', 'Ham', 0.5, 0.75, 1, '31f1fc90-b7c3-46c9-8d01-cccde2c89685', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('76226739-28c5-4aee-896d-4436d3fe0f3e', 'Sausage', 2, 3, 4, 'bc4fe65b-5251-453f-b4c4-4e20c3fe167e', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('8517e0e9-efe8-49cc-9259-3f61af124ebe', 'Feta Cheese', 2, 3, 4, 'bc4fe65b-5251-453f-b4c4-4e20c3fe167e', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('93921403-65ed-4634-8411-3b068460e38b', 'Olives', 2, 3, 4, 'bc4fe65b-5251-453f-b4c4-4e20c3fe167e', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('a701973d-7c55-4050-901b-f762066e9024', 'Tomatoes', 2, 3, 4, 'bc4fe65b-5251-453f-b4c4-4e20c3fe167e', '2018-02-05 12:28:36', '2018-02-05 12:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `toppingTypes`
--

CREATE TABLE `toppingTypes` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toppingTypes`
--

INSERT INTO `toppingTypes` (`id`, `name`, `createdAt`, `updatedAt`) VALUES
('31f1fc90-b7c3-46c9-8d01-cccde2c89685', 'Deluxe Toppings', '2018-02-05 12:28:35', '2018-02-05 12:28:35'),
('bc4fe65b-5251-453f-b4c4-4e20c3fe167e', 'Basic Toppings', '2018-02-05 12:28:35', '2018-02-05 12:28:35');

-- --------------------------------------------------------

--
-- Table structure for table `userRoles`
--

CREATE TABLE `userRoles` (
  `id` int(11) NOT NULL,
  `userId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `roleId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userRoles`
--

INSERT INTO `userRoles` (`id`, `userId`, `roleId`, `createdAt`, `updatedAt`) VALUES
(1, 'f82fc24a-ab0b-48f0-af3f-0aa28d1df3ef', '368ec8c9-c110-426f-bcc6-9174ac39962e', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
(2, 'd44c1b6a-a804-4dd8-8450-aa6a4093c8a9', 'f6c92029-c513-47e9-ba53-17ea867cd22b', '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
(3, 'bdd41557-23b2-48c7-90bd-f5464f29768b', '368ec8c9-c110-426f-bcc6-9174ac39962e', '2018-02-05 12:28:36', '2018-02-05 12:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `lastLoginDate` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `resetCode` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `gender`, `password`, `lastLoginDate`, `active`, `resetCode`, `createdAt`, `updatedAt`) VALUES
('bdd41557-23b2-48c7-90bd-f5464f29768b', 'Edith Kinya', 'edith.kinya@gmail.com', 1, '$2a$10$QtW9.uN3uz9kd5lOUKQb.uh3jEygoPOzusDZSGYLZflLnCVpLFvKK', NULL, 1, NULL, '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('d44c1b6a-a804-4dd8-8450-aa6a4093c8a9', 'Billy Ochingwa', 'billy.ochingwa@gmail.com', 1, '$2a$10$npIrecDZY8iUmIGsj7JiGeAZs7AkQARG8z8WtV2jXx4i4mHRfEPyu', NULL, 1, NULL, '2018-02-05 12:28:36', '2018-02-05 12:28:36'),
('f82fc24a-ab0b-48f0-af3f-0aa28d1df3ef', 'Amos Mwangi', 'amosmwa@gmail.com', 1, '$2a$10$kEabWScI2IAT.TCgtPwVM.XJVrLiOgMDpvjY2vKPVJ51pbmv8SOvW', NULL, 1, NULL, '2018-02-05 12:28:36', '2018-02-05 12:28:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orderLineItems`
--
ALTER TABLE `orderLineItems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pizzas`
--
ALTER TABLE `pizzas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toppingOrderLineItems`
--
ALTER TABLE `toppingOrderLineItems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toppings`
--
ALTER TABLE `toppings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toppingTypes`
--
ALTER TABLE `toppingTypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userRoles`
--
ALTER TABLE `userRoles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `userRoles`
--
ALTER TABLE `userRoles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
