# Mova Pay Code Test

## Getting Started
The Project has two folder, ui and backend, each containing the front-end and backend of tthe test respectively;

## To run :

1. go to backend and install deps with "npm install"
2. in the backend folder update the database credentials in config/config.json
3. import database mova.db
4. run with "node serve/" and open [http://localhost:8081](http://localhost:8081)

## The Front-End
1. Go navigate to the ui folder and install dependancies with "npm install" and bower "install"
2. To run you will need to install gulp, "npm install -g gulp-cli" and then run "gulp serve"
3. Edit field base_url in ui/src/app/app.module.js to set the url of the server
