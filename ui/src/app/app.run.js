(function() {
    'use strict';

    angular
        .module('app')
        .run(runFunction);

    /* @ngInject */
    function runFunction($rootScope, $state, $cookies, $http, UserService) {

        // default redirect if access is denied
        function redirectError() {
            $state.go('500');
        }

        // Check if user exists
        /*try{
            var cookieUser = $cookies.getObject('user');
        
            if(angular.isDefined(cookieUser)) {
                if(UserService.checkToken)            
            }else{
                $state.go('authentication.login')
            }
        }catch(e){
            console.log(e);

            $state.go('authentication.login')
        }*/    

        // watches

        // redirect all errors to permissions to 500
        var errorHandle = $rootScope.$on('$stateChangeError', redirectError);

        // remove watch on destroy
        $rootScope.$on('$destroy', function() {
            errorHandle();
        });
    }
})();
