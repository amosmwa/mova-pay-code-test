(function() {
    'use strict';

    angular
        .module('app.field')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.toppingTypes',{
                    url         : '/toppingTypes',
                    templateUrl : 'app/field/toppingTypes/toppingTypes.tmpl.html',
                    controller  : 'ToppingTypesController',
                    controllerAs: 'vm'
                })                


		/*triMenuProvider.addMenu({
		    name    : 'Fields',
		    icon    : 'zmdi zmdi-money-box',
		    type: 'dropdown',    	       
		    priority: 3.1,
	        children: [{
	            name: 'Topping Types',
	            state: 'triangular.toppingTypes',
	            type: 'link'
	        },
            {
                name: 'Toppings',
                state: 'triangular.toppings',
                type: 'link'
            },
            {
                name : 'Pizza',
                state : 'triangular.pizzas',
                type : 'link'
            }]
		});*/
    }
})();