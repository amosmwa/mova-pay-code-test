(function() {
    'use strict';

    angular
        .module('app.field')
        .controller('ToppingTypesController', ToppingTypesController);

    /* @ngInject */
    function ToppingTypesController($state, triSettings, ToppingTypeFactory, $mdToast, $document, $mdDialog) {
        var vm = this;

        vm.query = {
            page : 1,
            limit : 10
        }

        vm.triSettings = triSettings;
        vm.showFieldDialog = showFieldDialog;
        vm.showProgressBar = showProgressBar;
        


        function showFieldDialog(ev, item){
            $mdDialog
                .show({
                    controller : 'FieldDialogController',
                    templateUrl        : 'app/common/fieldDialog/fieldDialog.tmpl.html',
                    parent             : angular.element($document.find('#content-container')),
                    targetEvent        : ev,
                    clickOutsideToClose: false,
                    locals : {
                        data : item
                    }
                })
                .then(function(data){
                    if(_.isEmpty(item)){
                        ToppingTypeFactory
                            .add(data)
                            .then(function(response){
                                getData();
                            })
                            .catch(function(err){

                            })
                    }else if(!_.isEqual(item, data)){
                        ToppingTypeFactory
                            .update(data)
                            .then(function(response){
                                getData();
                            })
                            .catch(function(err){
                                
                            })
                    }
                })
        }

        function showProgressBar(val){
            vm.showProgress = val;
        }

        function getData(){
            ToppingTypeFactory
                .getAll(vm.query.page, vm.query.limit)
                .then(function(response){
                    vm.data = response;
                })
                .catch(function(err){

                })
        }
    }
})();
