(function() {
	
    'use strict';	
	angular
		.module('app.field')
		.factory('PizzaFactory', PizzaFactory);

		/* @ngInject */    	
		function PizzaFactory($http, API_CONFIG){
			var base_url = API_CONFIG.base_url + 'api/pizza';

			var service = {
				getAll  : getAll,
				listAll : listAll,
				getOne 	: getOne,
				add 	: add,
				update 	: update		
			}


			return service;

			function getAll(page, limit){
				return $http
		                .get(base_url+ '?page='+page+'&limit='+limit)
		                .then(getAllSuccessful)
		                .catch(getAllFailed);

				function getAllSuccessful(response){
					return response.data;
				}

				function getAllFailed(error){
					return error.data;
				}
			}

			function listAll(){
				return $http
		                .get(base_url)
		                .then(listAllSuccessful)
		                .catch(listAllFailed);

				function listAllSuccessful(response){
					return response.data;
				}

				function listAllFailed(error){
					return error.data;
				}
			}

			function getOne(id){
				return $http
		                .get(base_url+'/'+id)
		                .then(getOneSuccessful)
		                .catch(getOneFailed);

				function getOneSuccessful(response){
					return response.data;
				}

				function getOneFailed(error){
					return error.data;
				}
			}

			function add(data){
				return $http
		                .post(base_url, data)
		                .then(postSuccessful)
		                .catch(postFailed);

				function postSuccessful(response){
					return response.data;
				}

				function postFailed(error){
					return error.data;
				}
			}

			function update(data){
				return $http
		                .put(base_url, data)
		                .then(putSuccessful)
		                .catch(putFailed);

				function putSuccessful(response){
					return response.data;
				}

				function putFailed(error){
					return error.data;
				}
			}

			function _delete(id){
				return $http
						.delete(base_url+'/'+id)
						.then(deleteSuccessful)
						.catch(deleteFailed)

				function deleteSuccessful(response){
					return response.data;
				}

				function deleteFailed(error){
					return error.data;
				}
			}
		}
})();