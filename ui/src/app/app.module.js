(function() {
    'use strict';

    angular
        .module('app', [
            'ui.router',
            'triangular',
            'ngAnimate', 'ngCookies', 'ngSanitize', 'ngMessages', 'ngMaterial', 'ui.mask',
            'googlechart', 'chart.js', 'linkify', 'ui.calendar', 'angularMoment', 'textAngular',
            'uiGmapgoogle-maps', 'hljs', 'md.data.table', 'angularFileUpload', 'nvd3',
            'ngMaterialDatePicker',
            'pdf',           
            'app.permission',
            'app.common',                            
            'app.pos',
            'app.report',
            'app.field'

        ])        
        .constant('API_CONFIG', {                
            'base_url'  :  ''
            //'base_url'  :  'http://localhost:8081/'
        },
        'angularMomentConfig', {
            timezone: 'Africa/Nairobi'
        });
})();
