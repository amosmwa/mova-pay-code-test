(function() {
    'use strict';

    angular
        .module('app.pos')
        .controller('POSController', POSController);

    /* @ngInject */
    function POSController($scope, $state, triSettings, ToppingTypeFactory, PizzaFactory, UserService, ToppingFactory, OrderFactory, $mdToast, $document, $mdDialog) {
        var vm = this;

        vm.query = {
            page : 1,
            limit : 10
        }

        vm.triSettings = triSettings;      
        vm.showProgressBar = showProgressBar;
        //vm.showRecentTransActionsDialog = showRecentTransActionsDialog;
        vm.showOrderLineItemDialog = showOrderLineItemDialog;

        vm.addQuantity = addQuantity;
        vm.deductQuantity = deductQuantity;        
        vm.showConfirmDeleteDialog = showConfirmDeleteDialog;
        vm.clear = clear;
        vm.checkout = checkout;
        vm.showOutputDialog = showOutputDialog;

        vm.order = { orderLineItems : [], user : ''};

        vm.subtotal = 0.00; vm.gst= 0.00;
        
        PizzaFactory
            .listAll()
            .then(function(response){                
                vm.pizzas = response;
            })

        ToppingFactory
            .listAll()
            .then(function(response){               
                vm.toppings = response
            });

        function getRecent(){
            OrderFactory
                .getAll(vm.query.page, vm.query.limit)
                .then(function(response){                    
                    vm.orders = response;
                })
        }



       /* function showRecentTransActionsDialog(ev, item){
            $mdDialog
                .show({
                    controller : function($mdDialog, transaction){
                        var vm = this;
                        vm.transaction = transaction;
                        vm.cancel = cancel;

                        function cancel(){
                            $mdDialog.cancel();
                        }                        
                    },
                    controllerAs : 'vm',
                    templateUrl        : 'app/pos/dialog/transaction.tmpl.html',
                    parent             : angular.element($document.find('#content-container')),
                    targetEvent        : ev,
                    clickOutsideToClose: false,
                    locals : {
                        transaction : item
                    }
                })
                .then(function(data){
                    if(_.isEmpty(item)){
                        ToppingTypeFactory
                            .add(data)
                            .then(function(response){
                                getData();
                            })
                            .catch(function(err){

                            })
                    }else if(!_.isEqual(item, data)){
                        ToppingTypeFactory
                            .update(data)
                            .then(function(response){
                                getData();
                            })
                            .catch(function(err){
                                
                            })
                    }
                })
        }*/

        function showOrderLineItemDialog(ev, item, index){
            $mdDialog
                .show({
                    controller : function($scope, $mdDialog, oli, pizzas, toppings){
                        var vm = this;

                        vm.pizzas = pizzas;
                        vm.toppings = toppings;
                        vm.cancel = cancel;
                        vm.submit = submit;
                        vm.clear = clear;
                        vm.subtotal = 0.00;

                        if(_.isEmpty(oli)){
                            vm.title = "Add To Order";                            
                        }else{
                            vm.title = "Update to Order";
                            vm.oli = oli;
                        }

                        function cancel(){
                            $mdDialog.cancel();
                        }

                        function submit(){
                            vm.oli.subtotal = vm.subtotal;
                            vm.oli.quantity = 1;
                            $mdDialog.hide(vm.oli);
                        }

                        function clear(){
                            vm.oli = {};
                            vm.subtotal = 0.00;
                        }

                        $scope.$watch('vm.oli.pizza', function(newValue, oldValue){
                            if(!_.isEmpty(newValue)){
                                vm.subtotal += newValue.price;
                            }else if(_.isEmpty(newValue) && !_.isEmpty(oldValue)){
                                clear();
                            }
                        });

                        $scope.$watch('vm.oli.toppingOrderLineItems', function(newValue, oldValue){
                            if(!_.isEmpty(newValue)){
                                newValue.forEach(function(entry){
                                    vm.subtotal += parseInt(entry[''+vm.oli.pizza.name]);
                                })
                            }else if(_.isEmpty(newValue) && !_.isEmpty(oldValue)){
                                vm.subtotal = vm.oli.price;
                            }
                        })
                    },
                    controllerAs : 'vm',
                    templateUrl        : 'app/pos/dialog/orderLineItem.tmpl.html',
                    parent             : angular.element($document.find('#content-container')),
                    targetEvent        : ev,
                    clickOutsideToClose: false,
                    locals : {
                        oli : item,
                        pizzas : vm.pizzas,
                        toppings : vm.toppings
                    }
                })
                .then(function(data){                    
                    if(_.isEmpty(item)){
                        vm.order.orderLineItems.push(data);                        
                    }else if(!_.isEqual(data, item)){
                        vm.order.orderLineItems[index] = data;
                    }

                    calculateTotal()
                })
        }

        function showProgressBar(val){
            vm.showProgress = val;
        }

        function getData(){
            ToppingTypeFactory
                .getAll(vm.query.page, vm.query.limit)
                .then(function(response){
                    vm.data = response;
                })
                .catch(function(err){

                })
        }

        function getRecentTransactions(){

        }

        function addQuantity(index){    
            vm.order.orderLineItems[index].quantity +=  1;
            calculateTotal()
        }

        function deductQuantity(index){
            if(vm.order.orderLineItems[index].quantity == 1){
                deleteItem(index);
            }else{
                vm.order.orderLineItems[index].quantity -= 1;
            }

            calculateTotal();
        }

        function deleteItem(index){
             vm.order.orderLineItems.splice(index, 1);
             calculateTotal();
        }

        function showConfirmDeleteDialog(ev, index, clearOrder){
            $mdDialog
                .show({
                    controller : function($mdDialog, clearOrder){
                        var vm = this;

                        if(clearOrder){
                            vm.text = "Are you sure you want to clear?"
                        }else{
                            vm.text = "Are you sure you want to delete item?"
                        }
                       
                        vm.delete = function(){                           
                            $mdDialog.hide();
                        }

                        vm.cancel = function () {
                            $mdDialog.cancel();
                        };                        
                    },
                    controllerAs : 'vm',
                    templateUrl        : 'app/common/deleteDialog/deleteDialog.tmpl.html',
                    parent             : angular.element($document.find('#content-container')),
                    targetEvent        : ev,
                    clickOutsideToClose: false,
                    locals : {
                        clearOrder : clearOrder
                    }  
                })
                .then(function(data){
                    if(clearOrder){
                        clear();
                    }else{
                       deleteItem(index);
                    }
                })
        }

        function calculateTotal(){
            vm.subtotal = 0.00;
            vm.order.orderLineItems.forEach(function(entry){
                vm.subtotal += parseInt(entry.subtotal) * parseInt(entry.quantity);
            });

            vm.gst = vm.subtotal * 0.05;
        }

        function checkout(ev){
            vm.order.subtotal = vm.subtotal;
            vm.order.gst = vm.gst;
            vm.order.user = {id : 'a1f0e43c-05a4-4a6e-8569-ad796187e01c'}

            OrderFactory
                .add(vm.order)
                .then(function(response){
                    showOutputDialog(ev, null);
                    clear();
                    getRecent();
                })

            //showOutputDialog(ev)
        }


        function showOutputDialog(ev, order){
            $mdDialog
                .show({
                    controller : function($mdDialog, order){
                        var vm = this;
                        
                        function toWords (num) {
                           return _.capitalize(numberToWords.toWords(num))
                        }

                        vm.toWords = toWords;
                        vm.order = order;
                        vm.cancel = cancel;                        

                        function cancel(){
                            $mdDialog.cancel();
                        }
                    },
                    controllerAs : 'vm',
                    templateUrl        : 'app/pos/dialog/output.tmpl.html',
                    parent             : angular.element($document.find('#content-container')),
                    targetEvent        : ev,
                    clickOutsideToClose: false,
                    locals : {
                       order : (_.isEmpty(order) ? vm.order : order)
                    }
                })
        }

        function clear(){
            vm.order = { orderLineItems : [], user : ''};
            calculateTotal();
        }

        getRecent();
    
    }
})();
