(function() {
	
    'use strict';	
	angular
		.module('app.field')
		.factory('OrderFactory', OrderFactory);

		/* @ngInject */    	
		function OrderFactory($http, API_CONFIG){
			var base_url = API_CONFIG.base_url + 'api/order';


			return {
				getAll : getAll,
				getRecent : getRecent,
				add : add 
			};


			function getAll(page, limit){
				return $http.get(base_url+'?page='+page+'&limit='+limit)
						.then(function(response){
							return response.data
						})
						.catch(function(err){
							return err.data;
						})
			}

			function getRecent(start, end){

			}

			function add(order){
				return $http.post(base_url, order)
						.then(function(response){
							return response.data
						})
						.catch(function(err){
							return err.data;
						})

			}
		}	

})();