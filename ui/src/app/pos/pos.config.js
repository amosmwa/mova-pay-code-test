(function() {
    'use strict';

    angular
        .module('app.pos')
        .config(moduleConfig);

        /* @ngInject */
        function moduleConfig($stateProvider, triMenuProvider) {

            $stateProvider
                    .state('triangular.pos',{
                        url : '/pos',
                        templateUrl : 'app/pos/pos.tmpl.html',
                        controller : 'POSController',
                        controllerAs : 'vm',
                        data : {
                            layout : {                                
                                footer : false,
                                sideMenuSize : 'hidden'
                            }
                        }
                    });     


    		triMenuProvider.addMenu({
    		    name    : 'POS',
    		    icon    : 'zmdi zmdi-calendar-alt',    		    
    		    priority: 1.0,    	       
                state: 'triangular.pos',
                type: 'link'
    		});
        }
})();