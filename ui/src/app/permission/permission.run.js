(function() {
    'use strict';

    angular
        .module('app.permission')
        .run(permissionRun);

    /* @ngInject */
    function permissionRun($rootScope, $cookies, $state, PermissionStore, RoleStore, UserService) {
        // create permissions and add check function verify all permissions
        var permissions = ['viewReports', 'viewUsers', 'viewFields', 'viewPOS'];
        PermissionStore.defineManyPermissions(permissions, function (permissionName) {
            return UserService.hasPermission(permissionName);
        });

        // create roles for app
        RoleStore.defineManyRoles({            
            'ADMIN'    : ['viewReports', 'viewUsers', 'viewFields'],            
            'USER'          : ['viewPOS'],
        });


        ///////////////////////

        // default redirect if access is denied
        function accessDenied() {
            $state.go('401');
        }

        // watches

        // redirect all denied permissions to 401
        var deniedHandle = $rootScope.$on('$stateChangePermissionDenied', accessDenied);

        // remove watch on destroy
        $rootScope.$on('$destroy', function() {
            deniedHandle();
        });;
    }
})();
