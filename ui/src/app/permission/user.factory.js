(function() {
    'use strict';

    angular
        .module('app.permission')
        .factory('UserService', UserService);

    /* @ngInject */
    function UserService($q, $http, $cookies, RoleStore, API_CONFIG) {
        var currentUser = {
            displayName: 'Christos',
            username: 'christos',
            avatar: 'assets/images/avatars/avatar-5.png',
            roles: ['SUPERADMIN'],
            id : 1
        };

        var base_url = "http://localhost:8081/", 
        base_url2 = API_CONFIG.base_url + 'api/user';

        var service = {
            getCurrentUser      : getCurrentUser,
            getUsers            : getUsers,
            hasPermission       : hasPermission,
            login               : login,            
            logout : logout
        };

        return service;

        ///////////////

        function getCurrentUser() {
            return currentUser;
        }

        function getCurrentUserPromise(){
            return $q.when(currentUser);
        }

        function setCurrentUser(jwt, token){
            currentUser = {
                userId      : jwt.userId,
                token       : token,
                roles       : jwt.role.split("##").map(String),
                email       : jwt.email,
                name        : jwt.name,
                displayName : jwt.name
            };
            
            setHttpAuthToken();
            $cookies.putObject('user', currentUser);
        }

        function getUsers(page, limit){
            return $http.get(base_url2 + '?page='+page+'&limit='+limit)
                        .then(getSuccessful)
                        .catch(getFailed);
            function getSuccessful(response){
                return response.data;
            }

            function getFailed(error){
                return error.data;
            }
        }

        function getUser(id){
            return $http.get(base_url2 + '/'+id)
                        .then(getSuccessful)
                        .catch(getFailed);
            function getSuccessful(response){
                return response.data;
            }

            function getFailed(error){
                return error.data;
            }
        }   

        function searchUser(text){
            return $http.post(base_url2 + '/searchUser', text)
                        .then(getSuccessful)
                        .catch(getFailed);

            function getSuccessful(response){
                return response.data;
            }

            function getFailed(error){
                return error.data;
            }
        }

        function add(user){
            return $http.post(base_url2, user)
                        .then(postSuccessful)
                        .catch(postFailed);

            function postSuccessful(response){
                return response.data;
            }

            function postFailed(error){
                return error.data;
            }         
        }

        function edit(user){
            return $http.put(base_url2, user)
                        .then(postSuccessful)
                        .catch(postFailed);

            function postSuccessful(response){
                return response.data;
            }

            function postFailed(error){
                return error.data;
            }
        }

        function _delete(id){
            return $http.delete(base_url2+'/'+id)
                        .then(getSuccessful)
                        .catch(getFailed);

            function getSuccessful(response){
                return response.data;
            }

            function getFailed(error){
                return error.data;
            }    
        }     

        function hasPermission(permission) {
            var deferred = $q.defer();
            var hasPermission = false;

            // check if user has permission via its roles
            angular.forEach(currentUser.roles, function(role) {
                // check role exists
                if(RoleStore.hasRoleDefinition(role)) {
                    // get the role
                    var roles = RoleStore.getStore();

                    if(angular.isDefined(roles[role])) {
                        // check if the permission we are validating is in this role's permissions
                        if(-1 !== roles[role].validationFunction.indexOf(permission)) {
                            hasPermission = true;
                        }
                    }
                }
            });

            // if we have permission resolve otherwise reject the promise
            if(hasPermission) {
                deferred.resolve();
            }
            else {
                deferred.reject();
            }

            // return promise
            return deferred.promise;
        }
       
        function login(data) {
            return $http.post(base_url+'authenticate', data)
                            .then(authenticateSuccessful)
                            .catch(authenticateFailed);

                function authenticateSuccessful(response){
                    return response.data;
                }

                function authenticateFailed(error){
                    return error.data;
                }
        }

        function logout(){

        }

        function loginCookie(){
            currentUser =  $cookies.getObject('user');            
            setHttpAuthToken();
        }

        function setHttpAuthToken(){
            $http.defaults.headers.common['Authorization'] = currentUser.token;
        }

        function getToken(){
            return currentUser.token;
        }

       function forgotPass(email){
            return $http.post(base_url + 'resetPassword', email)
                        .then(postSuccessful)
                        .catch(postFailed);

            function postSuccessful(response){
                return response.data;
            }

            function postFailed(error){
                return error.data;
            }           
        }

        function changePass(pass){
            return $http.post(base_url + 'changePassword', pass)
                        .then(postSuccessful)
                        .catch(postFailed);

            function postSuccessful(response){
                return response.data;
            }

            function postFailed(error){
                return error.data;
            }           
        }

        function newPass(pass){
            return $http.post(base_url + 'newPass', pass)
                        .then(postSuccessful)
                        .catch(postFailed);

                function postSuccessful(response){
                    return response.data;
                }

                function postFailed(error){
                    return error.data;
                }           
        }
    }
})();
