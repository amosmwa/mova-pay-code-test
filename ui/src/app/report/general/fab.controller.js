(function() {
    'use strict';

    angular
        .module('app.report')
        .controller('GeneralFabController', GeneralFabController);

    /* @ngInject */
    function GeneralFabController($rootScope) {
        var vm = this;
        vm.showGeneralDateDialog = showGeneralDateDialog;

        ////////////////

        function showGeneralDateDialog($event) {
            $rootScope.$broadcast('showGeneralDateDialog', $event);
        }
    }
})();