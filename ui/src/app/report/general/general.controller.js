(function() {
    'use strict';

    angular
        .module('app.report')
        .controller('GeneralReportController', GeneralReportController);

    /* @ngInject */
    function GeneralReportController($rootScope, $scope, $document, $state, $mdDialog, $mdToast, ReportFactory) {
        
        var vm = this; 

        vm.dateRange = {
            start: moment().startOf('month'),
            end: moment().endOf('month')
        };

        vm.getData = getData;       
        vm.downloadMonthlyReport = downloadMonthlyReport;

        function getData(){
            $rootScope.$broadcast('reportProgressBar', true);
                      
            ReportFactory
                .getGeneralReport({start : vm.dateRange.start.format('YYYY-MM-DD')+" 00:00:00", end : vm.dateRange.end.format('YYYY-MM-DD')+" 23:59:59"})
                .then(function(result){
                    console.log(result);
                    vm.data = result;

                    $rootScope.$broadcast('reportProgressBar', false);
                })           
        }
        


        $rootScope.$on('showGeneralDateDialog', function(event, $event){ 

            $mdDialog.show({
                controller: 'DateChangeDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/report/general/dateChangeDialog.tmpl.html',
                locals: {
                    range: vm.dateRange
                },
                targetEvent: $event
            })
            .then(function() {
                // create new data
                getData();

                // pop a toast
                $mdToast.show(
                    $mdToast.simple()
                    .content('Date Range Updated')
                    .position('bottom right')
                    .hideDelay(2000)
                );
            });

        });

        var originatorEv;
        vm.openMenu = function($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };      

        function downloadMonthlyReport(ev, type){
            $mdDialog
                .show({
                    controller : function($scope, $mdDialog, $mdToast, DocumentFactory, type){
                        var vm = this;

                        vm.months = moment.months();

                        var years = moment().diff('2012-01-01', 'years'), end = years+2012+1;
                        vm.years =  _.range(2012, end);                        

                        vm.excel = {
                            down: function() {},
                            data: []
                        };

                        vm.printExcel = printExcel;
                        vm.close = close;

                        function printExcel(){

                            var month = vm.months.indexOf(vm.month) + 1,  lastDayOfMonth = moment('2012-'+month, "YYYY-M").daysInMonth();
                           if(type == 'pdf'){
                            DocumentFactory
                                .getMonthlyPDFReportData(month, vm.year, lastDayOfMonth)
                                .then(function(result){                                    
                                    pdfMake.createPdf(result).download("Report - "+moment(vm.year+'-'+month+'-'+lastDayOfMonth).format("MMMM YYYY")+'.pdf');
                                    close();
                                });                            

                           }else if(type == 'excel'){
                            DocumentFactory
                                .getMonthlyExcelReportData(month, vm.year, lastDayOfMonth)
                                .then(function(result){
                                    console.log(result);
                                    vm.excel.down(result);

                                    close();
                                })
                           }
                        }

                        function close(){
                            $mdDialog.cancel();
                        }
                    },
                    controllerAs       : 'vm',
                    templateUrl        : 'app/common/printReportDialog/printReportDialog.tmpl.html',
                    parent             : angular.element($document.find('#content-container')),
                    targetEvent        : ev,
                    locals : {
                        type : type
                    },
                    clickOutsideToClose: false  

                })
        }
       

        getData();
    }
})();