(function() {
    'use strict';

    angular
        .module('app.report')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
        .state('triangular.generalReport', {
            url : '/generalReport',            
            views: {
                '': {
                    templateUrl: 'app/report/general/general.tmpl.html',
                    controller: 'GeneralReportController',
                    controllerAs: 'vm'
                },
                'belowContent': {
                    templateUrl: 'app/report/general/fab-button.tmpl.html',
                    controller: 'GeneralFabController',
                    controllerAs: 'vm'
                }
            },
            data: {
                permissions: {
                    only: ['viewReports']
                }
            }           
        })
        .state('triangular.orderReport', {
            url : '/orderReport',
            controller :  'OrderController',
            controllerAs : 'vm',
            data: {
                permissions: {
                    only: ['viewReports']
                }
            } 
        });

        triMenuProvider.addMenu({
            name    : 'Reports',
            icon    : 'zmdi zmdi-graphic-eq',
            type: 'dropdown',              
            priority: 4.1,
            children: [{
                name: 'General Report',            
                state: 'triangular.generalReport',
                type: 'link'
            },{
               name: 'Order Report',            
               state: 'triangular.orderReport',
               type: 'link'
            }],
            permission  : 'viewReports'
        });
    }
})();
