(function() {
    'use strict';

    angular
        .module('app.report')
        .controller('GeneralReportController', GeneralReportController);

    /* @ngInject */
    function GeneralReportController($document, $state, $mdDialog, $mdToast, ReportFactory, UserFactory) {
    	var vm = this;    	

    	vm.getData = getData;
    	vm.clear = clear;

    	vm.query = {
	        start : moment().startOf('week').toDate(),
	        end : moment().endOf('week').toDate(),
	        filter : '',
	        limit : 30,
	        page : 1,
	        attendingUser : null           
	    }

    	UserFactory
    		.listAll()
    		.then(function(response){
    			vm.users = response;
    		})
    		.catch(function(err){

    		});

    	function getData(){    	  

    	    DashboardFactory
    	        .orders(vm.query)
    	        .then(function(response){                    
    	            vm.orders = response;
    	        })

    	}

    	function clear(){
    	    vm.query = {
    	        start : moment().startOf('week').toDate(),
    	        end : moment().endOf('week').toDate(),
    	        filter : '',
    	        limit : 30,
    	        page : 1,
    	        attendingUser : null           
    	    }

    	    getData();
    	}

    	
    }

})();