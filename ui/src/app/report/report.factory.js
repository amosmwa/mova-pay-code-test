(function (){
    'use strict';

    angular
        .module('app.report')
        .factory('ReportFactory', ReportFactory);   

     /* @ngInject */
    function ReportFactory($http, db, API_CONFIG){

        //var base_url  = "https://us-central1-ace-tech-d74a4.cloudfunctions.net/app/";
    	//var base_url  = "https://us-central1-aceapp-685c2.cloudfunctions.net/app";
        var base_url = API_CONFIG.base_url,

    	service = {
    		getGeneralReport : getGeneralReport
    	};

    	return service;

    	function getGeneralReport(data, token){    		
    		return $http
    				.post(base_url+'/generalReport', data)//, { 'headers' : {'Authorization' : 'Bearer '+token}})
    				.then(reqSuccessful)
    				.catch(reqFailed);
    	}



    	function reqSuccessful(response){    	  
    	    return response.data;
    	}

    	function reqFailed(error){
    		console.log(error.data);
    	    return error.data;
    	}
    }

})();