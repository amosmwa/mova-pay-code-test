(function ()
{
    'use strict';

    angular
        .module('app.common')
        .controller('DeleteDialogController', DeleteDialogController);

    /** @ngInject */    
    function DeleteDialogController($scope, $mdDialog, theScope, theFactory, id){   
    	
        var vm = this;

        theScope(false);
        
        function showProgressBar(value){
            vm.showProgress = value;
        }
        
        vm.delete = function(){
                showProgressBar(true);
                theFactory.delete(id)
                            .then(function(response){
                                showProgressBar(true);
                                $mdDialog.hide();
                            });
        }

        vm.cancel = function () {
            $mdDialog.cancel();
        };
    }

})();
