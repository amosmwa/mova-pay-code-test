(function ()
{
    'use strict';

    angular
        .module('app.common')
        .controller('LogsDialogController', LogsDialogController);

    /** @ngInject */    
    function LogsDialogController($mdDialog, CommonsFactory, type, id, title)
    {   
    	var vm = this;

        vm.cancel =  cancel;
        vm.form_title = title;

        vm.showProgress = true;

    	CommonsFactory
            .getLogs(type, id)
            .then(function(response){
                vm.data = response;
                vm.showProgress = false;
            });

        function cancel(){
            $mdDialog.cancel();
        }
    }

})();