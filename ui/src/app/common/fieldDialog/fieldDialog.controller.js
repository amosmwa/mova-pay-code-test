(function ()
{
    'use strict';

    angular
        .module('app.common')
        .controller('FieldDialogController', FieldDialogController);

    /** @ngInject */    
    function FieldDialogController($mdDialog, $mdToast, $state, $scope, data){
    	var vm = this;

    	if(_.isEmpty(data)){
            vm.title = "Add Field";           
        }else{
            vm.title= "Update Field";
            vm.data = data;
        }

        vm.close    = close;
        vm.submit   = submit;
        vm.showProgressBar = showProgressBar;
        
        function showProgressBar(value){
            vm.showProgress = value
        }        

        function close(){
            $mdDialog.cancel();
        }

        function submit(){
            $mdDialog
                .hide(vm.data);
        }
    }

})();