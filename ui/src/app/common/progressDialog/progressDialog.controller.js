(function ()
{
    'use strict';

    angular
        .module('app.common')
        .controller('ProgressDialogController', ProgressDialogController);

    /** @ngInject */    
    function ProgressDialogController($mdDialog, message)
    {   
    	var vm = this;

    	vm.message =  message;
    }

})();
